#ifndef INTERFACE_H
#define INTERFACE_H
#include <curses.h>
#include <string>
#include <vector>
using namespace std;


#define INFO_X_START 0
#define INFO_Y_START 60
#define INFO_HEIGHT 14
#define INFO_WIDTH 60

#define MENU_X_START 0
#define MENU_Y_START 0
#define MENU_HEIGHT 11
#define MENU_WIDTH 60

#define INPUT_X_START MENU_HEIGHT
#define INPUT_Y_START 0
#define INPUT_HEIGHT 3
#define INPUT_WIDTH 60

#define MAIN_MENU 0
#define CHOSE_TEMPERATURE 1
#define TURN_ON_DEVICE 2
#define TURN_OFF_DEVICE 3
#define UPDATE_TEMP_FEEDBACK 4
#define SUCESS 5
#define BAD_INPUT 6

#define USER_REFER 1
#define POTENCIOMETER_REFER 2
#define UART_DELAY 10000

#define COLOR_SELECTED 2
#define COLOR_UNSELECTED 1

#define OPTION_1 0
#define OPTION_2 1
#define OPTION_3 2
#define OPTION_4 3

#define MENU_LINES 6
#define INFO_LINES 25

class Info{
    private:
        WINDOW *window;
        int width;
        int heigth;
        bool* alarm_state;
        bool* sensor_detected;
    public:
        Info(bool* alarm_state, bool* sensor_detected);
        void update();
        int getWidth();
        int getHeight();
};

namespace menu_screen{
    struct option{
        string showing_text;
        void (*onSelect)(void);
        option(){
            showing_text = "";
            onSelect = [](){};
        }
        option(string _showing_text, void (*_onSelect)(void)){
            showing_text = _showing_text;
            onSelect = _onSelect;
        }
    };
}

class Input{
    private:
        void updateScreen();
        bool isActive;
        WINDOW *window;
    public:
        Input();
        Input(int x_start, int y_start);
        double getDouble();
        int getInt();
        void deactivate();
        void activate();
};

class Menu{
    private:
        WINDOW *window;
        Input input_screen;
        int width;
        int height;
        bool running;
        int current_option;
        bool* alarm_state;
        int current_screen;
        string log_name;
        void updateScreen();
    public:
        Menu(bool* alarm_state, string log_name);
        void update();
        bool isRunning();
        int getWidth();
        int getHeiht();
};

void showEndScreen();

void InterfaceInit();

#endif
