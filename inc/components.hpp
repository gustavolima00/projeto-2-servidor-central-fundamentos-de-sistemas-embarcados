#ifndef COMPONENTS_HPP
#define COMPONENTS_HPP
#include <string>

namespace components{
    extern const int LAMP1;
    extern const int LAMP2;
    extern const int LAMP3;
    extern const int LAMP4;
    extern const int AIR1;
    extern const int AIR2;
    extern const int MOTION1;
    extern const int MOTION2;
    extern const int OPENING1;
    extern const int OPENING2;
    extern const int OPENING3;
    extern const int OPENING4;
    extern const int OPENING5;
    extern const int OPENING6;

    struct components_t{
        float temperature, refer_temperature, humidity;
        bool lamp1, lamp2, lamp3, lamp4,
            air1, air2,
            motion1, motion2,
            opening1, opening2, opening3, opening4, opening5, opening6,
            auto_temperature;
    };
    
    void update_handler(int socket, void *ptr);
    void defaut_handler(int socket);
    components_t get_data();
    void turn_on(int component_id);
    void turn_off(int component_id);
    void switch_c(int component_id);
    void set_refer_temperature(float refer_temperature);
    std::string get_name(int device_id);
}
#endif
