#include <iostream>
#include "interface.hpp"
#include "network.hpp"
#include "components.hpp"
#include <signal.h>
#include <thread>
#include <sstream>
#include <iomanip>
#include "log.hpp"
using namespace std;

bool running;
bool alarm_state;
bool sensor_detected;
string log_name;

TCPServer *server;
/* Ncurses screens */
Menu *menu;
Info *info;

/* Thread variables */
int count;
thread menu_thread, info_thread, server_thread, alarm_thread;
bool mt_start, mt_finished;
bool it_start, it_finished;
bool st_start, st_finished;
bool at_start, at_finished;

/* Functions */
void stop_execution(int);
void sig_handler(int);
void connectionHandler(int);

int main()
{
	signal(SIGALRM, sig_handler);
	signal(SIGTERM, stop_execution);
	signal(SIGABRT, stop_execution);
	signal(SIGKILL, stop_execution);
	signal(SIGINT, stop_execution);
	log_name = initLog();

	running = true;
	server = new TCPServer(CENTRAL_SERVER_PORT.c_str(), connectionHandler);
	/* Init ncurses */
	InterfaceInit();

	/* Alloc screens */
	menu = new Menu(&alarm_state, log_name);
	info = new Info(&alarm_state, &sensor_detected);

	ualarm(100000, 100000);
	while (running)
		pause();
	endwin();
	delete menu;
	delete info;
}
void stop_execution(int signum)
{
	running = false;
}
void sig_handler(int signum)
{
	count = (count + 1) % 20;
	/* Updating info every 1 second */
	if (running)
	{
		if (not mt_start) // every 100 ms
		{
			mt_start = true;
			mt_finished = false;
			menu_thread = thread([]() {
				menu->update();
				mt_finished = true;
			});
		}
		if (not st_start)
		{
			st_start = true;
			st_finished = false;
			server_thread = thread([]() {
				server->acceptConnection();
				st_finished = true;
			});
		}
		if (alarm_state and sensor_detected and not at_start){
			at_start = true;
			at_finished = false;
			alarm_thread = thread([]() {
				system("omxplayer alarm.mp3 > /dev/null 2>&1");
				at_finished = true;
			});
		}
		if (count % 10 == 0 and not it_start) // every 1 second
		{
			it_start = true;
			it_finished = false;
			info_thread = thread([]() {
				info->update();
				it_finished = true;
			});
		}
	}
	/* Joins */
	if (mt_start and mt_finished)
	{
		menu_thread.join();
		mt_start = false;
	}
	if (st_start and st_finished)
	{
		server_thread.join();
		st_start = false;
	}
	if (at_start and at_finished)
	{	
		alarm_thread.join();
		at_start = false;
	}
	if (it_start and it_finished)
	{
		info_thread.join();
		it_start = false;
	}
}
void connectionHandler(int socket)
{
	uint8_t header;
	int rcv_bytes = recv(socket, &header, sizeof(header), 0);
	if (rcv_bytes < 0)
	{
		cerr << "Erro no recv()" << endl;
		return;
	}
	uint8_t RESPONSE_OK = 0x0, BAD_REQUEST = 0x1;
	switch (header)
	{
	case (0x0):
	{
		uint8_t component_id;
		int rcv_bytes = recv(socket, &component_id, sizeof(component_id), 0);
		if (rcv_bytes < 0)
		{
			cerr << "Erro no recv()" << endl;
			return;
		}
		send(socket, &RESPONSE_OK, sizeof(uint8_t), 0);
		sensor_detected = true;
		if(alarm_state){
			registerLog(log_name, "Acionamento do alarme", components::get_name(component_id));
		}
	}
	break;
	default:
	{
		send(socket, &BAD_REQUEST, sizeof(uint8_t), 0);
	}
	}
}
