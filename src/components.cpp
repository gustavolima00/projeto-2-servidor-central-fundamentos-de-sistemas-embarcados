#include "components.hpp"
#include "network.hpp"
#include <string>
#include <iostream>
using namespace std;

extern const int components::LAMP1 = 0x0;
extern const int components::LAMP2 = 0x1;
extern const int components::LAMP3 = 0x2;
extern const int components::LAMP4 = 0x3;
extern const int components::AIR1 = 0x4;
extern const int components::AIR2 = 0x5;
extern const int components::MOTION1 = 0x6;
extern const int components::MOTION2 = 0x7;
extern const int components::OPENING1 = 0x8;
extern const int components::OPENING2 = 0x9;
extern const int components::OPENING3 = 0x10;
extern const int components::OPENING4 = 0x11;
extern const int components::OPENING5 = 0x12;
extern const int components::OPENING6 = 0x13;

components::components_t components::get_data()
{
    components_t *response = new components_t;
    uint8_t header = 0x0;
    int clientSocket = send_message((char *)DISTRIBUTED_SERVER_IP.c_str(), (char *)DISTRIBUTED_SERVER_PORT.c_str(), (char *)&header, sizeof(uint8_t));
    components::update_handler(clientSocket, response);
    close(clientSocket);
    return *response;
}
void components::turn_on(int component_id){
    uint8_t header = 0x1;
    char buffer[2];
    buffer[0] = header;
    buffer[1] = component_id;
    send_message((char *)DISTRIBUTED_SERVER_IP.c_str(), (char *)DISTRIBUTED_SERVER_PORT.c_str(), buffer, 2, components::defaut_handler);
}
void components::turn_off(int component_id){
    uint8_t header = 0x2;
    char buffer[2];
    buffer[0] = header;
    buffer[1] = component_id;
    send_message((char *)DISTRIBUTED_SERVER_IP.c_str(), (char *)DISTRIBUTED_SERVER_PORT.c_str(), buffer, 2, components::defaut_handler);
}
void components::switch_c(int component_id){
    uint8_t header = 0x3;
    char buffer[2];
    buffer[0] = header;
    buffer[1] = component_id;
    send_message((char *)DISTRIBUTED_SERVER_IP.c_str(), (char *)DISTRIBUTED_SERVER_PORT.c_str(), buffer, 2, components::defaut_handler);
}
void components::set_refer_temperature(float refer_temperature){
    uint8_t header = 0x4;
    char buffer[5];
    buffer[0] = (char)header;
    memcpy(buffer+1, &refer_temperature, sizeof(float));
    send_message((char *)DISTRIBUTED_SERVER_IP.c_str(), (char *)DISTRIBUTED_SERVER_PORT.c_str(), buffer, sizeof(uint8_t)+sizeof(float), components::defaut_handler);
}
void components::defaut_handler(int socket){
    uint8_t header;
    int rcv_bytes = recv(socket, &header, sizeof(header), 0);
    if (rcv_bytes<0 or header != 0x0)
    {
        cerr << "Erro na comunicação com o servidor" << endl;
        return;
    }
}
void components::update_handler(int socket, void *ptr)
{
    components::components_t *response = (components::components_t *)ptr;
    uint8_t header;
    int rcv_bytes = recv(socket, &header, sizeof(header), 0);
    if (header != 0x0)
    {
        cerr << "Erro na comunicação com o servidor" << endl;
        return;
    }
    if (rcv_bytes < 0)
    {
        cerr << "Erro no recv()" << endl;
        return;
    }
    float f_buffer[3];
    rcv_bytes = recv(socket, f_buffer, sizeof(float) * 3, 0);
    if (rcv_bytes < 0)
    {
        cerr << "Erro no recv()" << endl;
        return;
    }
    response->temperature = f_buffer[0];
    response->refer_temperature = f_buffer[1];
    response->humidity = f_buffer[2];

    uint8_t buffer[15];
    rcv_bytes = recv(socket, buffer, sizeof(uint8_t) * 15, 0);
    if (rcv_bytes < 0)
    {
        cerr << "Erro no recv()" << endl;
        return;
    }
    response->lamp1 = buffer[0] == 0x1;
    response->lamp2 = buffer[1] == 0x1;
    response->lamp3 = buffer[2] == 0x1;
    response->lamp4 = buffer[3] == 0x1;

    response->air1 = buffer[4] == 0x1;
    response->air2 = buffer[5] == 0x1;

    response->motion1 = buffer[6] == 0x1;
    response->motion2 = buffer[7] == 0x1;

    response->opening1 = buffer[8] == 0x1;
    response->opening2 = buffer[9] == 0x1;
    response->opening3 = buffer[10] == 0x1;
    response->opening4 = buffer[11] == 0x1;
    response->opening5 = buffer[12] == 0x1;
    response->opening6 = buffer[13] == 0x1;

    response->auto_temperature = buffer[14] == 0x1;
}
string components::get_name(int device_id){
    switch(device_id){
        case components::LAMP1:
            return "Lampada 01 (Cozinha)";
        case components::LAMP2:
            return "Lampada 02 (Sala)";
        case components::LAMP3:
            return "Lampada 03 (Quarto 01)";
        case components::LAMP4:
            return "Lampada 04 (Quarto 02)";
        case components::AIR1:
            return "Ar-Condicionado 01 (Quarto 01)";
        case components::AIR2:
            return "Ar-Condicionado 02 (Quarto 02)";
        case components::MOTION1:
            return "Sensor de Presenca 01 (Sala)";
        case components::MOTION2:
            return "Sensor de Presenca 02 (Cozinha)";
        case components::OPENING1:
            return "Sensor Abertura 01 (Porta Cozinha)";
        case components::OPENING2:
            return "Sensor Abertura 02 (Janela Cozinha)";
        case components::OPENING3:
            return "Sensor Abertura 03 (Porta Sala)";
        case components::OPENING4:
            return "Sensor Abertura 04 (Janela Sala)";
        case components::OPENING5:
            return "Sensor Abertura 05 (Janela Quarto 01)";
        case components::OPENING6:
            return "Sensor Abertura 06 (Janela Quarto 02)";
        default: 
        return "";
    }
}