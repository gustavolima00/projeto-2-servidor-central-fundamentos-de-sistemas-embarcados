#include <iostream>
#include "interface.hpp"
#include "components.hpp"
#include "network.hpp"
#include <iomanip>
#include <unistd.h>
#include <curses.h>
#include <fstream>
#include <string>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include "log.hpp"
using namespace std;

Info::Info(bool* alarm_state, bool* sensor_detected){
    this->alarm_state = alarm_state;
    this->sensor_detected = sensor_detected;
    this->width = INFO_WIDTH;
    this->heigth = INFO_LINES + 7;
    this->window = newwin(this->heigth, this->width, INFO_X_START, INFO_Y_START);
    box(this->window , 0, 0);
    this->update();
}

int Info::getWidth(){
    return this->width;
}

int Info::getHeight(){
    return this->heigth;
}

void Info::update(){
     wclear(this->window);
    box(this->window , 0, 0);
    mvwprintw(this->window, 1, 2, "Informacoes do sistema");
    try{
        components::components_t component = components::get_data();
        vector<stringstream> streams(INFO_LINES);
        /* Checking sensors */
        bool active = false;
        active |= component.motion1;
        active |= component.motion2;
        active |= component.opening1;
        active |= component.opening2;
        active |= component.opening3;
        active |= component.opening4;
        active |= component.opening5;
        active |= component.opening6;
        if(!active) *this->sensor_detected = false;
        /* Updating info screen */
        int i = 0;
        streams[i++] << fixed << setprecision(2) << "Temperature: " << component.temperature << " ºC";
        streams[i++] << fixed << setprecision(2) << "Temperatura de referencia: " << component.refer_temperature << " ºC";
        streams[i++] << fixed << setprecision(2) << "Humidade: " << component.humidity << " %";
        streams[i++] << "";
        streams[i++] << "1  - Lampada 01 (Cozinha): " << (component.lamp1 ? "LIGADA" : "DESLIGADA");
        streams[i++] << "2  - Lampada 02 (Sala): " << (component.lamp2 ? "LIGADA" : "DESLIGADA");
        streams[i++] << "3  - Lampada 03 (Quarto 01): " << (component.lamp3 ? "LIGADA" : "DESLIGADA");
        streams[i++] << "4  - Lampada 04 (Quarto 02): " << (component.lamp4 ? "LIGADA" : "DESLIGADA");
        streams[i++] << "";
        streams[i++] << "5  - Ar-Condicionado 01 (Quarto 01): " << (component.air1 ? "LIGADO" : "DESLIGADO");
        streams[i++] << "6  - Ar-Condicionado 02 (Quarto 02): " << (component.air2 ? "LIGADO" : "DESLIGADO");
        streams[i++] << "";
        streams[i++] << "7  - Sensor de Presença 01 (Sala): " << (component.motion1 ? "LIGADO" : "DESLIGADO");
        streams[i++] << "8  - Sensor de Presença 02 (Cozinha): " << (component.motion2 ? "LIGADO" : "DESLIGADO");
        streams[i++] << "";
        streams[i++] << "9  - Sensor Abertura 01 (Porta Cozinha): " << (component.opening1 ? "ATIVADO" : "DESATIVADO");
        streams[i++] << "10 - Sensor Abertura 02 (Janela Cozinha): " << (component.opening2 ? "ATIVADO" : "DESATIVADO");
        streams[i++] << "11 - Sensor Abertura 03 (Porta Sala): " << (component.opening3 ? "ATIVADO" : "DESATIVADO");
        streams[i++] << "12 - Sensor Abertura 04 (Janela Sala): " << (component.opening4 ? "ATIVADO" : "DESATIVADO");
        streams[i++] << "13 - Sensor Abertura 05 (Janela Quarto 01): " << (component.opening5 ? "ATIVADO" : "DESATIVADO");
        streams[i++] << "14 - Sensor Abertura 06 (Janela Quarto 02): " << (component.opening6 ? "ATIVADO" : "DESATIVADO");
        streams[i++] << "";
        streams[i++] << "15 - Controle automático da temperatura: " << (component.auto_temperature ? "ATIVADO" : "DESATIVADO");
        streams[i++] << "";
        streams[i++] << "16 - Alarme: " << (*this->alarm_state ? "ATIVADO" : "DESATIVADO");

    
        for(int j=0; j<INFO_LINES; ++j)
            mvwprintw(this->window, 4+j, 2, "%s", streams[j].str().c_str());
    }
    catch(server_error& e){
        mvwprintw(this->window, this->heigth/2, 2, "%s", e.get_message());
    }
	wrefresh(this->window);
}
Input::Input(){
    this->window = newwin(INPUT_HEIGHT, INPUT_WIDTH, INPUT_X_START, INPUT_Y_START);
    this->isActive = false;
    this->updateScreen();
}
Input::Input(int x_start, int y_start){
    this->window = newwin(INPUT_HEIGHT, INPUT_WIDTH, x_start, y_start);
    this->isActive = false;
    this->updateScreen();
}

void Input::updateScreen(){
    wclear(this->window);
    if(this->isActive){
        box(this->window , 0, 0);
        mvwprintw(this->window, 1, 2, "> ");
    }
    wrefresh(this->window);
}

void Input::activate(){
    this->isActive = true;
    this->updateScreen();
}

void Input::deactivate(){
    this->isActive = false;
    this->updateScreen();
}

double Input::getDouble(){
    if(!this->isActive) return -1;
    double value;
	wscanw(this->window, "%lf", &value);
    this->updateScreen();
    return value;
}
int Input::getInt(){
    if(!this->isActive) return -1;
    int value;
	wscanw(this->window, "%d", &value);
    this->updateScreen();
    return value;
}

Menu::Menu(bool* alarm_state, string log_name){
    this->log_name = log_name;
    this->input_screen = Input(MENU_LINES+7, INPUT_Y_START);
    this->input_screen.deactivate();
    this->current_screen = MAIN_MENU;
    this->alarm_state = alarm_state;
    this->current_option = 0;
    this->height = MENU_LINES+7;
    this->width = MENU_WIDTH;
    this->window = newwin(this->height, this->width, MENU_X_START, MENU_Y_START);
    box(this->window , 0, 0);
    keypad(this->window, TRUE);
    this->updateScreen();
}

int Menu::getHeiht(){
    return this->height;
}

int Menu::getWidth(){
    return this->width;
}

void Menu::updateScreen(){
    wclear(this->window);
    box(this->window , 0, 0);
    if(this->current_screen == MAIN_MENU){
        mvwprintw(this->window, 1, 20, "Menu de acoes");
        int i = 0;
        if(this->current_option == 0){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4+i, 2, "* Ligar dispositivo");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 4+i, 2, "  Ligar dispositivo");
        }
        ++i;
        if(this->current_option == 1){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4+i, 2, "* Desligar dispositivo");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 4+i, 2, "  Desligar dispositivo");
        }
        ++i;
        if(this->current_option == 2){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4+i, 2, "* Escolher temperatura de referencia");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 4+i, 2, "  Escolher temperatura de referencia");
        }
        ++i;
        if(this->current_option == 3){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4+i, 2, "* Ligar alarme");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 4+i, 2, "  Ligar alarme");
        }
        ++i;
        if(this->current_option == 4){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4+i, 2, "* Desligar alarme");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 4+i, 2, "  Desligar alarme");
        }
        ++i;
        if(this->current_option == 5){
            wattron(window, COLOR_PAIR(COLOR_SELECTED));
            mvwprintw(this->window, 4+i, 2, "* Sair");
            wattroff(window, COLOR_PAIR(COLOR_SELECTED));
        }
        else{
            mvwprintw(this->window, 4+i, 2, "  Sair");
        }
        ++i;
    }
    else if(this->current_screen == CHOSE_TEMPERATURE){
        mvwprintw(this->window, this->height/2-1, this->width/2 - 10, "Entre com o valor da");
        mvwprintw(this->window, this->height/2, this->width/2 - 13, "temperatura e aperte enter");
    }
    else if(this->current_screen == TURN_ON_DEVICE){
        mvwprintw(this->window, this->height/2, this->width/2 - 21, "Insira o ID do dispositivo a ser ligado");
    }
    else if(this->current_screen == TURN_OFF_DEVICE){
        mvwprintw(this->window, this->height/2, this->width/2 - 21, "Insira o ID do dispositivo a ser desligado");
    }
    else if(this->current_screen == UPDATE_TEMP_FEEDBACK){
        mvwprintw(this->window, this->height/2, this->width/2 - 21, "A temperatura de refefencia foi atualizada");
    }
    else if(this->current_screen == SUCESS){
        mvwprintw(this->window, this->height/2, this->width/2 - 18, "Sua acao foi processada com sucesso");
    }
    else if(this->current_screen == BAD_INPUT){
        mvwprintw(this->window, this->height/2, this->width/2 - 7, "Valor invalido");
    }
    wrefresh(this->window);
}

void Menu::update(){
    if(this->current_screen == MAIN_MENU){
        int key = wgetch(this->window);
        int n = MENU_LINES;
        if(key == KEY_DOWN){
            this->current_option += 1;
            this->current_option %= n;
        }
        if(key == KEY_UP){
            this->current_option += n-1;
            this->current_option %= n;
        }
        if(key == KEY_RIGHT or  key == '\n' or key == KEY_ENTER){
            try{
                if(this->current_option == 0){
                    this->current_screen = TURN_ON_DEVICE;
                }
                else if(this->current_option == 1){
                    this->current_screen = TURN_OFF_DEVICE;
                }
                else if(this->current_option == 2){
                    this->current_screen = CHOSE_TEMPERATURE;
                }
                else if(this->current_option == 3){
                    *this->alarm_state = true;
                    registerLog(log_name, "Ligar alarme", "Alarme");
                }
                else if(this->current_option == 4){
                    *this->alarm_state = false;
                    registerLog(log_name, "Desligar alarme", "Alarme");
                }
                else if(this->current_option == 5){
                    kill(getpid(), SIGINT);
                }
            }
            catch(server_error& e){
            }
        }
    }
    else if(current_screen == CHOSE_TEMPERATURE){
        this->input_screen.activate();
        double temp = this->input_screen.getDouble();
        try{
            components::set_refer_temperature(temp);
            registerLog(log_name, "Escolha da temperatua de referencia", to_string(temp));
        }
        catch(server_error& e){

        }
        this->current_screen = UPDATE_TEMP_FEEDBACK;
    }
    else if(current_screen == TURN_ON_DEVICE){
        this->input_screen.activate();
        int device_id = this->input_screen.getInt();
        if(device_id<1 or device_id>6){ 
            this->current_screen = BAD_INPUT;
        }
        else{
            try{
                components::turn_on(device_id-1);
                registerLog(log_name, "Ligar dispositivo", components::get_name(device_id-1));
            }
            catch(server_error& e){
            }
            this->current_screen = SUCESS;
        }
    }
    else if(current_screen == TURN_OFF_DEVICE){
        this->input_screen.activate();
        int device_id = this->input_screen.getInt();
        if(device_id<1 or device_id>6){ 
            this->current_screen = BAD_INPUT;
        }
        else{
            try{
                components::turn_off(device_id-1);
                registerLog(log_name, "Desligar dispositivo", components::get_name(device_id-1));
            }
            catch(server_error& e){
            }
            this->current_screen = SUCESS;
        }
        
    }
    else if(current_screen == UPDATE_TEMP_FEEDBACK or this->current_screen==BAD_INPUT or this->current_screen == SUCESS){
        this->input_screen.deactivate();
        sleep(2);
        this->current_screen = MAIN_MENU;
    }
    this->updateScreen();
}

void showEndScreen(){
    int row, col;
    getmaxyx(stdscr,row,col);
    WINDOW *window = newwin(row, col, 0, 0);
    wclear(window);
    box(window , 0, 0);
    mvwprintw(window, row/2, col/2-15, "Programa encerrado com sucesso");
    mvwprintw(window, row/2+1, col/2-11, "Aperte enter para sair");
    wrefresh(window);
    getch();
    clear();
    refresh();
    endwin();
}

void InterfaceInit(){
    initscr();
    if(has_colors()){
        start_color();
    }
    else{

    }
	curs_set(0);
	echo();
	cbreak();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_BLACK, COLOR_WHITE);

	refresh();
    int row, col;
    getmaxyx(stdscr,row,col);
    WINDOW *window = newwin(row, col, 0, 0);
    wclear(window);
    box(window , 0, 0);
    mvwprintw(window, row/2, col/2-30, "Para uma melhor experiencia aumente o tamanho do seu terminal");
    mvwprintw(window, row/2+1, col/2-16, "Aperte uma tecla para para continuar");
    refresh();
    wrefresh(window);
    getch();
    wclear(window);
    wrefresh(window);
}
