#include "network.hpp"
#include <iostream>
using namespace std;

extern const string DISTRIBUTED_SERVER_IP = "192.168.0.52";
extern const string DISTRIBUTED_SERVER_PORT = "10125";
extern const string CENTRAL_SERVER_IP = "192.168.0.53";
extern const string CENTRAL_SERVER_PORT = "10025";

server_error::server_error(const char* s){
    this->message = s;
}
const char* server_error::get_message(){
    return this->message;
}

TCPServer::TCPServer(const char* port, void (*connectionHandler)(int)){
    this->connectionHandler = connectionHandler;
    this->servidorPorta = atoi(port);
    // Abrir Socket
    if((this->servidorSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        throw server_error("Falha no socket do Servidor");
    // Montar a estrutura sockaddr_in
    memset(&this->servidorAddr, 0, sizeof(this->servidorAddr)); // Zerando a estrutura de dados
    this->servidorAddr.sin_family = AF_INET;
    this->servidorAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    this->servidorAddr.sin_port = htons(this->servidorPorta);

    // Bind
    if(bind(this->servidorSocket, (struct sockaddr *) &this->servidorAddr, sizeof(this->servidorAddr)) < 0)
        throw server_error("Falha no Bind");

    // Listen
    if(listen(this->servidorSocket, 10) < 0)
        throw server_error("Falha no Listen");
}
TCPServer::~TCPServer(){
	close(this->servidorSocket);
}
void TCPServer::acceptConnection(){
    struct sockaddr_in clienteAddr;
    unsigned int clienteLength = sizeof(clienteAddr);
    int socketCliente;
    if((socketCliente = accept(this->servidorSocket, 
                                (struct sockaddr *) &clienteAddr, 
                                &clienteLength)) < 0){
        throw server_error("Falha no Accept");
    }
    this->connectionHandler(socketCliente);
    close(socketCliente);
}
int send_message(char* server_ip, char* server_port, char* message, size_t tamanhoMensagem){
    int clienteSocket;
	struct sockaddr_in servidorAddr;
	unsigned short servidorPorta = atoi(server_port);
    // Criar Socket
    if((clienteSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
        throw server_error("Erro no socket()");
    }
    // Construir struct sockaddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr)); // Zerando a estrutura de dados
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = inet_addr(server_ip);
	servidorAddr.sin_port = htons(servidorPorta);
    
    // Connect
	if(connect(clienteSocket, (struct sockaddr *) &servidorAddr, 
							sizeof(servidorAddr)) < 0)
    {
        throw server_error("Falha ao se comunicar com o servidor");
    }
    int recv_bytes = send(clienteSocket, message, tamanhoMensagem, 0);
    if(recv_bytes<0){
        throw server_error("Erro no recebimento da mensagem send()");
    }
    return clienteSocket;
}
void send_message(char* server_ip, char* server_port, char* message, size_t tamanhoMensagem, void (*response_handler)(int)){
    int clienteSocket = send_message(server_ip, server_port, message, tamanhoMensagem);
    response_handler(clienteSocket);
	close(clienteSocket);
}
