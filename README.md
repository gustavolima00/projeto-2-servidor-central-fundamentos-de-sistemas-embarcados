# Projeto 2 (Servidor Central) - Fundamentos de sistemas embarcados

## Servidor central

O servidor central consistem em uma interface com o controle das lampadas, ar condicionado e da temperatura de referência
Alem de ser possível ativar e desativar o alarme do sistema.
Para o funcionamento adequado do servidor centrarl o servidor distribuído deve estar rodando

### Formato da requisição

#### Cabeçalho 

(1 byte) Tipo de requisição

- 0x0 Alerta para ativação de dispositivo

#### Corpo

(1 byte) ID do Dispositivo

- 0x6 : Sensor de Presença 01 (Sala)
- 0x7 : Sensor de Presença 02 (Cozinha)
- 0x8 : Sensor Abertura 01 (Porta Cozinha)
- 0x9 : Sensor Abertura 02 (Janela Cozinha)
- 0xA : Sensor Abertura 03 (Porta Sala)
- 0xB : Sensor Abertura 04 (Janela Sala)
- 0xC : Sensor Abertura 05 (Janela Quarto 01)
- 0xD : Sensor Abertura 06 (Janela Quarto 02)

### Formato da resposta

#### Cabeçalho

(1 byte) Resultado da requisição

0x0 : Sucesso na requisição
0x1 : Erro no formato da mensagem
0x2 : Erro interno do servidor


## Servidor distribuído

O link para o git do servidor distribuído é o seguinte [servidor distribuído](https://gitlab.com/gustavolima00/projeto-2-fundamentos-de-sistemas-embarcados)